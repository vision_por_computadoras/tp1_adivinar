import random

def adivina(intentos):
    int_og=intentos
    numero = random.randint(0,100)
    while intentos>=0:
        intentos-=1
        guess=input("Ingrese un numero entre 0 y 100: ")
        if int(guess)!=numero:
            print("Valor incorrecto")
            print(f"(El valor es {numero})\n")
        else:
            print(f"Felicitaciones! Adivinaste el numero en {int_og-intentos} intentos")
            break
    else:
        print("Te quedaste sin intentos. Juega otra vez!")

adivina(3)
